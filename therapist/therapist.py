class Therapist(object):
    def __init__(self, user_id):
        self.user_id = user_id
        self.state = ""

    def start(self, text=None, **kwargs):
        if self.state == "feltbad":
            self.state = "started_feltbad"
            return "Are you feeling any better?", ["Yes", "No"]
        else:
            self.state = "started"
            return "How are you feeling?", ["Good", "Bad"]

    def handle_input(self, text=None, **kwargs):
        if self.state == "started":
            if text == "Good":
                self.state = "good0"
                return "Why do you feel good?", None
            elif text == "Bad":
                self.state = "bad0"
                return "Why do you feel bad?", None
            else:
                return "Sorry, I didn't understand that.", None

        elif self.state == "started_feltbad":
            if text == "Yes":
                self.state = "good0"
                return "What happened to make you feel better?", None
            elif text == "No":
                self.state = "bad0"
                return "Why not?", None
            else:
                return "Sorry, I didn't understand that.", None

        elif self.state == "good0":
            self.state = "good1"
            return "What can you learn from this?", None
        elif self.state == "good1":
            self.state = "feltgood"
            return "I'm glad you're feeling well.", None

        elif self.state == "bad0":
            self.state = "bad1"
            return "Is there anything you can do about this?", ["Yes", "No"]
        elif self.state == "bad1":
            if text == "Yes":
                self.state = "bad2yes"
                return "What's that?", None
            elif text == "No":
                self.state = "bad2no"
                return "What about someone else?", ["Yes", "No"]
            else:
                return "Sorry, I didn't understand that.", None
        elif self.state == "bad2yes":
            self.state = "feltbad"
            return "Give it a try.", None
        elif self.state == "bad2no":
            if text == "Yes":
                self.state = "feltbad"
                return "Reach out to them, if possible.", None
            elif text == "No":
                self.state = "feltbad"
                return "I'm sorry I couldn't help.", None
            else:
                return "Sorry, I didn't understand that.", None
        elif self.state == "":
            return "I'm sorry, but you'll need to /start me first.", None
